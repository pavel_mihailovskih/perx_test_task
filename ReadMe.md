Описание задачи в файле task.md

---

Для генерации ключей необходимо запустить менеджмент команду (generate_keys)

`manage.py generate_keys`



####Отчет по работе сервиса:
*	Структура адресов апи - http://joxi.ru/L21G41au8KPDkr?d=1

*	Выдача уникальных ключей - http://joxi.ru/12M5ladiM5e852?d=1

*	Погашение ключа. 

Помечает ключ как использованный - http://joxi.ru/l2ZxEJyUwopkPr?d=1

Повторно погасить ключ нельзя - http://joxi.ru/a2XnE8kc16RZbr?d=1

Нельзя погасить ключ, если он не был предварительно выдан клиенту - 
http://joxi.ru/ZrJkGL5U94YW3m?d=1

*	Проверка ключа. 

Возвращает информацию информацию о ключе: 

не выдан - http://joxi.ru/YmE3GQjs0RN9BA?d=1

выдан - http://joxi.ru/ZrJkGL5U94Ye3m?d=1

погашен - http://joxi.ru/E2ppXPwc93RXj2?d=1

*	Информация о количестве оставшихся, не выданных ключах - http://joxi.ru/YmE3GQjs0RNDBA?d=1
