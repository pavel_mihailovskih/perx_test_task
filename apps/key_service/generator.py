import itertools
import string

from .constants import STATUS_CHOICE_NOT_ISSUED
from .models import Key


class KeyGenerator(object):
    """
    Генерация ключей.
    По умолчанию: ключи состоят из букв латинского языка в нижнем и верхнем регистре и цифр, длинна - 4 символа
    """

    def __init__(self, required_chars=None, length=4):
        self.required_chars = required_chars or [string.ascii_lowercase, string.ascii_uppercase, string.digits]
        self.length = length or 4

    def get_unique_char_set(self) -> list:
        """
        Cписок уникальных перестановок надоров символов для генерации ключа
        [('AB', 'ab', '12', 'ab'), ('ab', 'AB', '12', 'ab'), ...... ('12', 'ab', 'AB','ab')]
        """
        items = []
        for part in self.required_chars:
            for item in itertools.permutations(self.required_chars + [part], self.length):
                if item not in items:
                    items.append(item)
        return items

    def generate_key_permutation(self, item):
        """
        Генерация ключей из перестановки символов
        """
        key_list = []
        for key in itertools.product(*item):
            key_list.append(Key(key=''.join(key), status=STATUS_CHOICE_NOT_ISSUED))
        Key.objects.bulk_create(key_list)

    def generate(self):
        for item in self.get_unique_char_set():
            self.generate_key_permutation(item)
