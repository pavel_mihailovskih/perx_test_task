from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError

from .constants import STATUS_CHOICE_ISSUED, STATUS_CHOICE_REDEEMED, STATUS_CHOICE_NOT_ISSUED


class CheckActivation:
    """
    Базовый класс для проверки возможности активации ключа
    """

    def get_error_class(self, is_rest=True):
        return ValidationError if is_rest else RuntimeError

    def check(self, *args, **kwargs):
        raise NotImplementedError()


class CheckStatusIsRedeemed(CheckActivation):
    """
    Проверяем: статус ключа, и может ли он быть активирован.
    Нельзя повторно активировать ключи.
    """

    def check(self, key, is_rest):
        if key.status == STATUS_CHOICE_REDEEMED:
            error_class = self.get_error_class(is_rest=is_rest)
            raise error_class(_('Ключ не может быть повторно активирован'))

        return True


class CheckStatusIsIssued(CheckActivation):
    """
    Проверяем: статус ключа, и может ли он быть активирован.
    Активировать можно только выданный ключ STATUS_CHOICE_ISSUED
    """

    def check(self, key, is_rest):
        if key.status == STATUS_CHOICE_ISSUED:
            return True

        error_class = self.get_error_class(is_rest=is_rest)
        raise error_class(_('Ключ имеет не верный статус, должен быть "Не выдан"'))


class CheckStatusIsNotIssued(CheckActivation):
    """
    Проверяем: статус ключа, и может ли он быть активирован.
    Нельзя активировать не выданный ключ.
    """

    def check(self, key, is_rest):
        if key.status == STATUS_CHOICE_NOT_ISSUED:
            error_class = self.get_error_class(is_rest=is_rest)
            raise error_class(_('Ключ не выдан, поэтому не может быть активирован.'))

        return True


class CheckActivationKey:
    def __init__(self):
        self._checks = []

    def add_check(self, check_instance):
        self._checks.append(check_instance)

    def check(self, key, is_rest):
        results = []

        for check in self._checks:
            result = check.check(key=key, is_rest=is_rest)
            results.append(result)

        return all(results)


check_activation_key = CheckActivationKey()
check_activation_key.add_check(CheckStatusIsRedeemed())
check_activation_key.add_check(CheckStatusIsNotIssued())
check_activation_key.add_check(CheckStatusIsIssued())
