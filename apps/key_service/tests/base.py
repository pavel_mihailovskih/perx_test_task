from django.test.testcases import TestCase
from rest_framework.exceptions import ValidationError

from ..constants import STATUS_CHOICE_ISSUED, STATUS_CHOICE_REDEEMED, STATUS_CHOICE_NOT_ISSUED
from ..models import Key


class TestKeyModel(TestCase):

    def test_key_mark_issued(self):
        key = Key.objects.create(key='Aaa11', status=STATUS_CHOICE_NOT_ISSUED)
        self.assertEqual(key.status, STATUS_CHOICE_NOT_ISSUED)
        key.mark_issued_key()
        self.assertEqual(key.status, STATUS_CHOICE_ISSUED)

    def test_key_activate_error_is_rest(self):
        key = Key.objects.create(key='Bbb1', status=STATUS_CHOICE_REDEEMED)
        with self.assertRaises(ValidationError):
            key.activate()

    def test_key_activate_error_is_not_rest(self):
        key = Key.objects.create(key='Bbb1', status=STATUS_CHOICE_REDEEMED)
        with self.assertRaises(RuntimeError):
            key.activate(is_rest=False)

    def test_activate_not_issued(self):
        key = Key.objects.create(key='Bbb1', status=STATUS_CHOICE_REDEEMED)
        with self.assertRaises(ValidationError):
            key.activate()

    def test_get_not_issued_key(self):
        key1 = Key.objects.create(key='Avv1', status=STATUS_CHOICE_NOT_ISSUED)
        self.assertEqual(Key.objects.get_not_issued_key(), key1)

    def test_get_number_keys_not_issued(self):
        Key.objects.create(key='Avv1', status=STATUS_CHOICE_NOT_ISSUED)
        self.assertEqual(Key.objects.get_number_keys_not_issued(), 1)

        Key.objects.create(key='Avv2', status=STATUS_CHOICE_NOT_ISSUED)
        Key.objects.create(key='Avv3', status=STATUS_CHOICE_NOT_ISSUED)
        self.assertEqual(Key.objects.get_number_keys_not_issued(), 3)
