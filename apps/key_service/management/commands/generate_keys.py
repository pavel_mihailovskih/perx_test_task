from django.core.management.base import BaseCommand

from apps.key_service.generator import KeyGenerator


class Command(BaseCommand):
    help = 'Generate keys'

    def handle(self, *args, **options):
        print('start')
        KeyGenerator().generate()
        print('stop')
