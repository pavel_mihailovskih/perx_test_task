from django.db import models

from .constants import STATUS_CHOICE_NOT_ISSUED


class KeyQuerySet(models.QuerySet):

    def _all_available(self):
        return self.filter(status=STATUS_CHOICE_NOT_ISSUED)

    def get_not_issued_key(self):
        return self._all_available().order_by('?').first()

    def get_number_keys_not_issued(self):
        return self._all_available().count()


class KeyManager(models.Manager):
    pass
