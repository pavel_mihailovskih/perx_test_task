from django.contrib import admin

from .models import Key


@admin.register(Key)
class KeyAdmin(admin.ModelAdmin):
    list_display = ['key', 'status']
    list_filter = ['status']
    search_fields = ['key']

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False
