from django.db import models
from django.utils.translation import ugettext_lazy as _

from .utils import check_activation_key
from .managers import KeyManager, KeyQuerySet
from .constants import STATUS_CHOICES, STATUS_CHOICE_REDEEMED, STATUS_CHOICE_ISSUED


class Key(models.Model):
    """
    Модель для уникальных ключей
    """
    key = models.CharField(_('Ключ'), max_length=4, db_index=True, unique=True)
    status = models.PositiveSmallIntegerField(_('Статус'), choices=STATUS_CHOICES)

    objects = KeyManager.from_queryset(KeyQuerySet)()

    class Meta:
        verbose_name = 'ключ'
        verbose_name_plural = 'ключи'

    def __unicode__(self):
        return self.key

    def activate(self, is_rest=True):
        """
        Изменили статус ключа на Погашен.
        """
        if check_activation_key.check(key=self, is_rest=is_rest):
            self.status = STATUS_CHOICE_REDEEMED
            self.save()

    def mark_issued_key(self):
        """
        Изменили статус ключа на Выдан.
        """
        self.status = STATUS_CHOICE_ISSUED
        self.save()
        return self
