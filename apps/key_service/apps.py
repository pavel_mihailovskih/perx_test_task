from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class KeyServiceConfig(AppConfig):
    name = 'apps.key_service'
    verbose_name = _('Key service')
