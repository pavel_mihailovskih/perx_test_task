from rest_framework import serializers

from apps.key_service.models import Key
from apps.key_service.constants import STATUS_CHOICES


class OnlyKeySerializer(serializers.ModelSerializer):
    class Meta:
        model = Key
        fields = ['key']
        read_only = '__all__'

    def give_key(self):
        return self.instance.mark_issued_key()


class KeyStatusMixin:
    def get_current_status(self, obj):
        return next(filter(lambda s: s[0] == obj.status, STATUS_CHOICES))

    def get_status(self, obj):
        _, text = self.get_current_status(obj)
        return text


class KeyStatusSerializer(KeyStatusMixin, serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = Key
        fields = ['status']
        read_only = '__all__'


class KeyActivateSerializer(KeyStatusMixin, serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    class Meta:
        model = Key
        fields = ['key', 'status']
        read_only = '__all__'

    def activate(self):
        return self.instance.activate()
