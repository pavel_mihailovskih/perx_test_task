import django_filters

from apps.key_service.models import Key


class KeyFilter(django_filters.FilterSet):
    class Meta:
        model = Key
        fields = ['key']
