from django.urls import path

from .views import KeyGiveApi, KeyCheckApi, KeyActivateApi, KeysInfoApi


urlpatterns = [
    path('give/', KeyGiveApi.as_view()),
    path('check/', KeyCheckApi.as_view()),
    path('activate/', KeyActivateApi.as_view()),
    path('info/', KeysInfoApi.as_view()),
]
