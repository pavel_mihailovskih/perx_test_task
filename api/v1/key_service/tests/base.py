from django.test.testcases import TestCase
from rest_framework.test import APIClient

from apps.key_service.constants import STATUS_CHOICE_ISSUED, STATUS_CHOICE_REDEEMED, STATUS_CHOICE_NOT_ISSUED
from apps.key_service.models import Key


class TestKeySericeApi(TestCase):
    def setUp(self):
        super(TestKeySericeApi, self).setUp()
        self.key_list = ['XXx1', 'XXx2', 'XXx3', 'XXx4']
        self.api_client = APIClient()

    def create_keys_instances(self, status=None):
        status = status or STATUS_CHOICE_NOT_ISSUED
        Key.objects.bulk_create(list(
            map(
                lambda x: Key(key=x, status=status),
                self.key_list
            )
        ))

    def test_key_give_api_not_allow_method(self):
        self.create_keys_instances()

        request = self.api_client.get('/api/v1/keys/give/', content_type='application/json')
        self.assertEqual(request.status_code, 405)

        request = self.api_client.put('/api/v1/keys/give/', content_type='application/json')
        self.assertEqual(request.status_code, 405)

        request = self.api_client.patch('/api/v1/keys/give/', content_type='application/json')
        self.assertEqual(request.status_code, 405)

        request = self.api_client.delete('/api/v1/keys/give/', content_type='application/json')
        self.assertEqual(request.status_code, 405)

        for index, _ in enumerate(self.key_list*2, 1):
            if index <= len(self.key_list):
                self.api_client.post('/api/v1/keys/give/', content_type='application/json')
            else:
                with self.assertRaises(RuntimeError):
                    self.api_client.post('/api/v1/keys/give/', content_type='application/json')

    def test_key_give_api_allow_method(self):
        self.create_keys_instances()

        response = self.api_client.post('/api/v1/keys/give/', content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn(response.data['key'], self.key_list)
        self.assertEqual(Key.objects.get(key=response.data['key']).status, STATUS_CHOICE_ISSUED)

    def test_key_check_api(self):
        self.create_keys_instances()
        response = self.api_client.get('/api/v1/keys/check/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['key'], self.key_list[0])
        self.assertEqual(response.data['status']['value'], STATUS_CHOICE_NOT_ISSUED)

        response = self.api_client.get('/api/v1/keys/check/')
        self.assertEqual(response.status_code, 400)

    def test_key_check_error_api(self):
        self.create_keys_instances()
        response = self.api_client.get('/api/v1/keys/check/')
        self.assertEqual(response.status_code, 400)

        response = self.api_client.get('/api/v1/keys/check/', data={'key': 4565})
        self.assertEqual(response.status_code, 404)

        response = self.api_client.post('/api/v1/keys/check/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 405)

        response = self.api_client.put('/api/v1/keys/check/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 405)

        response = self.api_client.patch('/api/v1/keys/check/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 405)

        response = self.api_client.delete('/api/v1/keys/check/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 405)

    def test_key_activate_api(self):
        self.create_keys_instances()
        response = self.api_client.post('/api/v1/keys/give/')
        key = response.data['key']
        response = self.api_client.put('/api/v1/keys/activate/', data={'key': key})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Key.objects.get(key=key).status, STATUS_CHOICE_REDEEMED)

        response = self.api_client.post('/api/v1/keys/give/')
        key = response.data['key']
        response = self.api_client.patch('/api/v1/keys/activate/', data={'key': key})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Key.objects.get(key=key).status, STATUS_CHOICE_REDEEMED)

    def test_key_activate_errors_api(self):
        self.create_keys_instances()
        response = self.api_client.put('/api/v1/keys/activate/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 400)

        response = self.api_client.get('/api/v1/keys/activate/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 405)
        response = self.api_client.post('/api/v1/keys/activate/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 405)
        response = self.api_client.delete('/api/v1/keys/activate/', data={'key': self.key_list[0]})
        self.assertEqual(response.status_code, 405)

    def test_keys_info_api(self):
        self.create_keys_instances()
        response = self.api_client.get('/api/v1/keys/info/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Key.objects.get(status=STATUS_CHOICE_NOT_ISSUED).count(), response.data['not_issued'])

    def test_keys_info_errors_api(self):
        self.create_keys_instances()

        response = self.api_client.put('/api/v1/keys/info/')
        self.assertEqual(response.status_code, 405)
        response = self.api_client.patch('/api/v1/keys/info/')
        self.assertEqual(response.status_code, 400)
        response = self.api_client.post('/api/v1/keys/info/')
        self.assertEqual(response.status_code, 405)
        response = self.api_client.delete('/api/v1/keys/info/')
        self.assertEqual(response.status_code, 405)
