from django.db import transaction
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django_filters import rest_framework as filters
from rest_framework import generics, views
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from apps.key_service.models import Key
from .filters import KeyFilter
from .serializers import OnlyKeySerializer, KeyStatusSerializer, KeyActivateSerializer


class KeyGiveApi(views.APIView):
    queryset = Key.objects.none()
    serializer_class = OnlyKeySerializer

    def post(self, *args, **kwargs):
        """
        Выдача ключей
        """
        not_issued_key = Key.objects.get_not_issued_key()
        if not not_issued_key:
            raise RuntimeError(_('Нет свободных ключей, все выданы или активированы.'))

        serializer = self.serializer_class(instance=not_issued_key)

        with transaction.atomic():
            serializer.give_key()

        return Response(serializer.data)


class KeyCheckApi(generics.ListAPIView, generics.GenericAPIView):
    """
    Проверка ключа. Возвращает информацию информацию о ключе: не выдан, выдан, погашен.
    """
    queryset = Key.objects.all()
    serializer_class = KeyStatusSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = KeyFilter

    def check_required_query_param(self, value):
        """
        Проверяем обязательного query параметра в запросе.
        По нему осуществляется выборка объекта из БД
        """

        if not value:
            raise ValidationError(_('key - обязательный query параметр.'))

        return value

    def get_instance(self):
        param = self.request.GET.get('key')
        self.check_required_query_param(param)
        return get_object_or_404(self.filter_queryset(self.get_queryset()), key=param)

    def list(self, request, *args, **kwargs):
        instance = self.get_instance()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class KeyActivateApi(generics.UpdateAPIView, generics.GenericAPIView):
    """
    Активация ключа
    """
    queryset = Key.objects.all()
    serializer_class = KeyActivateSerializer

    def update(self, request, *args, **kwargs):
        instance = get_object_or_404(self.get_queryset(), key=self.request.data.get('key'))
        serializer = self.get_serializer(instance=instance)

        with transaction.atomic():
            serializer.activate()

        return Response(serializer.data)


class KeysInfoApi(views.APIView):

    def get(self, *args, **kwargs):
        """
        Информация о количестве ключей
        """
        return Response({
            'not_issued': Key.objects.get_number_keys_not_issued()
        })
