from django.urls import path, include


urlpatterns = [
    path('keys/', include('api.v1.key_service.urls')),
]
